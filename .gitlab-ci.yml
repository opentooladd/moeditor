# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Container Scanning customization: https://docs.gitlab.com/ee/user/application_security/container_scanning/#customizing-the-container-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence

stages:          # List of stages for jobs, and their order of execution
  - build
  - release
  - test
  - deploy

sast:
  stage: test
include:
- template: Security/SAST.gitlab-ci.yml

cache:
    key: $CI_COMMIT_REF_SLUG
    paths:
        - node_modules/
        - src-tauri/target/

variables:
  GIT_SUBMODULE_STRATEGY: normal
  GIT_SUBMODULE_DEPTH: 1
  GIT_SUBMODULE_FORCE_HTTPS: "true"

build-linux:       # This job runs in the build stage, which runs first.
  image:  ubuntu:focal # Ubuntu 20.04
  stage: build
  cache:
    key: $CI_COMMIT_REF_SLUG
    paths:
        - .apt/
        - .npm/
        - .cargo/
        - node_modules/
        - src-tauri/target/
  artifacts:
    name: "path-manager_release_$CI_COMMIT_REF_SLUG"
    paths:
        - static_website.zip
        - src-tauri/target/release/bundle/appimage/*.AppImage
        - src-tauri/target/release/bundle/deb/*.deb
    reports:
      # To ensure we've access to this file in the next stage
      dotenv: build_linux.env
  before_script:
    - echo $CI_JOB_ID
    # Writing GE_JOB_ID variable to environment file, will need the value in the next stage.
    - echo BUILD_LINUX_JOB_ID=$CI_JOB_ID >> build_linux.env
  script:
    # share apt-installs
    -  rm -f /etc/apt/apt.conf.d/docker-clean
     - mkdir .apt && mkdir /var/cache/apt/archives && mount --bind .apt /var/cache/apt/archives/
    # install tauri dependencies
    # see: https://www.garybell.co.uk/fixing-gitlab-pipeline-stuck-on-tzdata-configuration/
    - DEBIAN_FRONTEND=noninteractive apt-get update -yqq
    - DEBIAN_FRONTEND=noninteractive apt-get install -yqq libgtk-3-dev libwebkit2gtk-4.0-dev libappindicator3-dev librsvg2-dev git build-essential patchelf curl wget make zip openssl libssl-dev
    # Install Node Version Manager (NVM) so we can change the node version 
    - curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
    # source nvm script
    - "source ~/.nvm/nvm.sh"
    - nvm --version
    # install latest node
    - nvm install node
    - node -v
    - npm -v
    # install rust
    - curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
    - source "$HOME/.cargo/env"
    - rustc --version
    # install project dependencies and cache it
    - npm ci --cache .npm --prefer-offline
    # build project
    - make build-tauri-release
    - zip -r static_website.zip dist/
    - echo PROJECT_VERSION=$(npm pkg get version) >> build_linux.env

# @TODO
# Add Windows build

release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  script:
    - echo 'running release_job'
    - echo 'Previous Job ID is printed below'
    - echo $PROJECT_VERSION
  # Specifying that this job requires artifacts from the previous job to succeed
  needs:
    - job: build-linux
      artifacts: true
  release:
    name: 'Release Executables $CI_COMMIT_SHORT_SHA'
    description: 'Created using the release-cli'
    # tag_name is a mendatory field and can not be an empty string
    tag_name: '$CI_COMMIT_SHORT_SHA'
    assets:
      links:
        - name: 'Static Website'
          url: 'https://gitlab.com/opentooladd/$CI_PROJECT_NAME/-/jobs/$BUILD_LINUX_JOB_ID/artifacts/raw/static_website.zip'
        - name: 'Linux AppImage'
          url: 'https://gitlab.com/opentooladd/$CI_PROJECT_NAME/-/jobs/$BUILD_LINUX_JOB_ID/artifacts/raw/src-tauri/target/release/bundle/appimage/${CI_PROJECT_NAME}_${PROJECT_VERSION}_amd64.AppImage'
        - name: 'Debian package'
          url: 'https://gitlab.com/opentooladd/$CI_PROJECT_NAME/-/jobs/$BUILD_LINUX_JOB_ID/artifacts/raw/src-tauri/target/release/bundle/deb/${CI_PROJECT_NAME}_${PROJECT_VERSION}_amd64.deb'
  only:
    # Can be removed if you want to trigger the pipeline for merge request or other branches
    - main

# unit-test-job:   # This job runs in the test stage.
#   stage: test    # It only starts when the job in the build stage completes successfully.
#   dependencies:
#     - build-linux
#   script:
#     - npm run test:unit:ci

# deploy-linux-job:      # This job runs in the deploy stage.
#   stage: deploy  # It only runs when *both* jobs in the test stage complete successfully.
#   environment: production
#   script:
#     - echo "Deploying application..."
#     - echo "Application successfully deployed."
