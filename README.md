# TODO

- [x] Flex panel
- [x] Font Panel
- [ ] Animation
    - WIP
- [ ] Events
    - [ ] Page Events ?
- [ ] Functions

- [ ] Génération HTML
    - [x] Écrire page html avec structure static
    - [ ] Écrire CSS associé
        - [x] CSS basique
        - [ ] Media Queries
    - [ ] Écrire Javascript associé
        - [ ] Anime.js
        - [ ] Events
        - [ ] Function

- [ ] Tests
    - [ ] Inputs
    - [ ] Notifications
    - [ ] Panels
    - [ ] App
    - [ ] Store

## Dependencies

- [Vue-color-input](https://github.com/gVguy/vue-color-input)
- [JSZip](https://stuk.github.io/jszip/documentation/examples.html)
- [Vue3-Tabs](https://github.com/HJ29/vue3-tabs)
- [vue3-file-toolbar](https://github.com/motla/vue-file-toolbar-menu)
- [@jledentu/vue-finder](https://github.com/jledentu/vue-finder)
- [@rwh/keystrokes](https://github.com/RobertWHurst/Keystrokes)
- [ImmutableJS](https://immutable-js.com/)
- [Tauri](https://tauri.app)
- [Vue](https://vuejs.org)

# Tauri + Vue 3 + TypeScript

This template should help get you started developing with Vue 3 and TypeScript in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Recommended IDE Setup

- [VS Code](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) + [Tauri](https://marketplace.visualstudio.com/items?itemName=tauri-apps.tauri-vscode) + [rust-analyzer](https://marketplace.visualstudio.com/items?itemName=rust-lang.rust-analyzer)

## Type Support For `.vue` Imports in TS

Since TypeScript cannot handle type information for `.vue` imports, they are shimmed to be a generic Vue component type by default. In most cases this is fine if you don't really care about component prop types outside of templates. However, if you wish to get actual prop types in `.vue` imports (for example to get props validation when using manual `h(...)` calls), you can enable Volar's Take Over mode by following these steps:

1. Run `Extensions: Show Built-in Extensions` from VS Code's command palette, look for `TypeScript and JavaScript Language Features`, then right click and select `Disable (Workspace)`. By default, Take Over mode will enable itself if the default TypeScript extension is disabled.
2. Reload the VS Code window by running `Developer: Reload Window` from the command palette.

You can learn more about Take Over mode [here](https://github.com/johnsoncodehk/volar/discussions/471).
