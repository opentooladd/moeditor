module.exports = {
  extends: [
    // add more generic rulesets here, such as:
    // "eslint:recommended",
    "plugin:vue/vue3-recommended",
    // 'plugin:vue/recommended' // Use this if you are using Vue.js 2.x.
  ],
  rules: {
    // override/add rules settings here, such as:
    // 'vue/no-unused-vars': 'error'
    "quotes": ["error", "double"],
    "comma-style": ["error", "last"]
  },
  env: {
    browser: true,
    node: true
  },
  plugins: ["@typescript-eslint"],
  parser: "vue-eslint-parser",
  parserOptions: {
    parser: "@typescript-eslint/parser",
    sourceType: "module"
  }
}
