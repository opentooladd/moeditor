use serde::{Serialize, Deserialize};
use std::convert::{From};
// use tauri::api::dialog::FileDialogBuilder;
use tauri::InvokeError;

use thiserror::Error;

#[derive(Error, Serialize, Deserialize, Debug)]
pub enum WriteTreeError {
    #[error("InvokeError: `{0}`")]
    InvokeError(String),
}

impl From<InvokeError> for WriteTreeError {
    fn from(item: InvokeError) -> Self {
        WriteTreeError::InvokeError(format!("InvokeError: {:?}", item))
    }
}

#[tauri::command]
pub fn write_tree(tree: String, css: String) -> Result<(), WriteTreeError> {
    println!("{}", tree);
    println!("{}", css);
    Ok(())
}
