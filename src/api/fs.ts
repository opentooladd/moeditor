import JSZip from "jszip";
import { saveAs } from "file-saver";
// import { invoke } from "@tauri-apps/api/tauri";
import { save } from '@tauri-apps/api/dialog';
import { writeBinaryFile } from '@tauri-apps/api/fs';


// Start file download.
// download("hello.txt","This is the content of my file :)");

export const write_tree = async () => {
  const fontLinks = Array.from(document.head.querySelectorAll("[is-google-font-link='']")).reduce((acc, el) => {
    acc += el.outerHTML + "\n"
    return acc
  }, "");
  const treeString = window.document.querySelector("#main-scene").innerHTML.replaceAll(" selected", "");
  const styleSheet: CSSStyleSheet = Array.from(document.styleSheets).find((stylesheet) => stylesheet.title === "project-css");
  const css = Array.from(styleSheet.cssRules).map((rule: CSSRule) => rule.cssText).join("\n");
  const zip = new JSZip();
  const htmlTemplate = `<!DOCTYPE html>
<html lang="en">
<meta charset="UTF-8">
<title>Page Title</title>
<meta name="viewport" content="width=device-width,initial-scale=1">
${fontLinks}
<link rel="stylesheet" href="./style.css">
<body>

${treeString}

</body>
</html>`;
  zip.file("index.html", htmlTemplate);
  // pour le chargement d’un projet, garder le style inline
  zip.file("style.css", css);

  if (window.__TAURI__) {
    // Learn more about Tauri commands at https://tauri.app/v1/guides/features/command
    const path = await save({
      defaultPath: "project.zip"
    });

    const blob = await zip.generateAsync({type:"uint8array"});
    return writeBinaryFile(path, blob)
  } else { 
    const blob = await zip.generateAsync({type:"blob"});
    return saveAs(blob, "project.zip")
  }
}
