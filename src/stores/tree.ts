import { defineStore } from "pinia"
import { useCommandsStore } from "@/stores/commands";

const treeToList = (tree: HTMLElement, init = true) => {
   const result = [];
   
   if (init) result.push(tree);
   const children = Array.from(tree.children) || [];
   children.forEach(child => {
     const childResult = treeToList(child as HTMLElement);
     result.push(...childResult)
   })

   return result;
}

interface TreeStoreState {
  elements: HTMLElement[];
  selected: HTMLElement | null
  selectedStyle: CSSStyleDeclaration | null
  commandStore: any
}

export const useTreeStore = defineStore("tree", {
  state: (): TreeStoreState => ({
    elements: [] as HTMLElement[],
    selected: null,
    selectedStyle: null,
    commandStore: useCommandsStore()
  }),
  getters: {
    // this is to trigger a refresh on undo / redo
    historizedSelected() {
      return {
        selected: this.selected,
        selectedStyle: this.selectedStyle,
        history: this.commandStore.position
      }
    }
  },
  actions: {
    scan() {
      const scene = document.querySelector("#main-scene");
      this.elements = scene ? treeToList(scene as HTMLElement, false) : []
    },
    select(element: HTMLElement | null) {
      let elementStyle: CSSStyleDeclaration | null = null;
      if (element) {
        const id = element.getAttribute("data-id");
        const selector = `[data-id="${id}"]`;
        const styleSheet: CSSStyleSheet = Array.from(document.styleSheets).find((stylesheet) => stylesheet.title === "project-css");
        elementStyle = (Array.from(styleSheet.cssRules).find((rule : CSSStyleRule) => rule.selectorText === selector) as CSSStyleRule)?.style;
      } 
      this.selected = element;
      this.selectedStyle = elementStyle;
    }
  }
})
