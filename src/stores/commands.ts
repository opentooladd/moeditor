import { defineStore } from "pinia"
import Command from "@/models/Command"

enum Status {
  TOP = "TOP",
  HISTORY = "HISTORY",
}

interface CommandState {
  commands: Command[];
  position: number;
  status: Status;
}

interface CommandArgs {
  name: string;
  execute: () => void;
  undo: () => void;
}

export const useCommandsStore = defineStore("commands", {
  state: (): CommandState => ({
    commands: [],
    position: -1,
    status: Status.TOP,
  }),
  getters: {
    last: (state) => state.commands[state.commands.length - 1],
    first: (state) => state.commands[0],
  },
  actions: {
    shift() {
      return this.commands.shift()
    },
    pop() {
      return this.commands.pop()
    },
    splice({ index, count = 1 }: { index: number; count: number }) {
      return this.commands.splice(index, count)
    },
    "position:reset"() {
      this.position = this.commands.length - 1
    },
    "position:increment"() {
      return this.position <= this.commands.length - 1
        ? (this.position += 1)
        : null
    },
    "position:decrement"() {
      return this.position >= 0 ? (this.position -= 1) : null
    },
    reset(initial: Command[] = []) {
      this.commands = initial
      this.status = Status.TOP
    },
    "status:set"(status: Status): string {
      this.status = status
      return this.status
    },
    new(command: CommandArgs) {
      if (this.status === Status.HISTORY) {
        this["reset"](this.commands.slice(0, this.position + 1))
      }
      this.commands.push(new Command(command))
      this["position:reset"]()
      return this["execute:last"]({ new: true })
    },
    "execute:last"(args?: any) {
      return new Promise((resolve, reject) => {
        let result
        try {
          if (this.status !== Status.TOP) this["position:increment"]()
          if (
            (this.commands[this.position] && this.status !== Status.TOP) ||
            (args && args.new)
          ) {
            result = this.commands[this.position].execute(args)
            if (result instanceof Promise) {
              return result
                .then((res) => {
                  if (this.position === this.commands.length - 1) {
                    this["status:set"](Status.TOP)
                  }
                  resolve(res)
                })
                .catch((e) =>
                  e ? reject(e) : reject(new Error("top of history"))
                )
            }
            if (this.position === this.commands.length - 1) {
              this["status:set"](Status.TOP)
            }
            return resolve(result)
          }
          return reject(new Error("top of history"))
        } catch (e) {
          return reject(e)
        }
      })
    },
    "undo:last"(args?: any) {
      return new Promise((resolve, reject) => {
        let result
        try {
          if (this.position >= this.commands.length)
            this["position:decrement"]()
          if (this.commands[this.position]) {
            result = this.commands[this.position].undo(args)
            if (this.status !== Status.HISTORY)
              this["status:set"](Status.HISTORY)
          } else {
            return reject(new Error("end of history"))
          }
          this["position:decrement"]()
        } catch (e) {
          return reject(e)
        }
        return resolve(result)
      })
    },
  },
})
