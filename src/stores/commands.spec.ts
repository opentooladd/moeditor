import { useCommandsStore } from "./commands"
import { setActivePinia, createPinia } from "pinia"
import { beforeEach, afterEach, describe, it, expect, vi } from "vitest"
import Command from "@/models/Command"

describe("store/command", () => {
  let store: any

  beforeEach(() => {
    // creates a fresh pinia and make it active so it's automatically picked
    // up by any useStore() call without having to pass it to it:
    // `useStore(pinia)`
    setActivePinia(createPinia())
    store = useCommandsStore()
  })

  afterEach(() => {
    store["reset"]()
  })

  it("should create an empty store", () => {
    expect(store.commands).toBeDefined()
    expect(store.commands).toBeInstanceOf(Array)
  })

  it("should add a new command", () => {
    expect(() =>
      store["new"]({
        name: "createObject",
        execute: vi.fn(),
        undo: vi.fn(),
      })
    ).not.toThrow()
    expect(store["last"]).toBeInstanceOf(Command)
    expect(store["last"].name).toEqual("createObject")
    expect(() =>
      store["new"]({
        name: "createObject2",
        execute: vi.fn(),
        undo: vi.fn(),
      })
    ).not.toThrow()
    expect(() =>
      store["new"]({
        name: "createObject3",
        execute: vi.fn(),
        undo: vi.fn(),
      })
    ).not.toThrow()

    expect(store["last"]).toBeInstanceOf(Command)
    expect(store["last"].name).toEqual("createObject3")
  })

  it("should execute last command", async () => {
    store["new"]({
      name: "createObject",
      execute: vi.fn((test) => test),
      undo: vi.fn(),
    })
    store["position:reset"]()
    const result = await store["execute:last"]({ new: true })

    expect(store["last"]).toBeInstanceOf(Command)
    expect(store["last"].execute).toHaveBeenCalled()
    expect(result).toEqual({ new: true })
  })
  it("should reject if last command execute throws", async () => {
    store["new"]({
      name: "createObject",
      execute: vi.fn(),
      undo: vi.fn(),
    })

    await expect(store["execute:last"]("test")).rejects.toThrow(new Error("top of history"))
  })

  it("should undo last command", async () => {
    await store["new"]({
      name: "createObject",
      execute: vi.fn(),
      undo: vi.fn((test) => test),
    })
    const result = await store["undo:last"]("test")

    expect(store["last"]).toBeInstanceOf(Command)
    expect(store["last"].undo).toHaveBeenCalled()
    expect(result).toEqual("test")
  })
  it("should reject if last command undo throws", async () => {
    store["new"]({
      name: "createObject",
      execute: vi.fn(),
      undo: () => {
        throw new Error()
      },
    })

    let result
    try {
      result = await store["undo:last"]()
    } catch (e) {
      expect(store["last"]).toBeInstanceOf(Command)
      expect(result).not.toBeDefined()
    }
  })

  it("should remove the first element of the state.commands", () => {
    store["new"]({ name: "Command1" })
    store["new"]({ name: "Command2" })
    store["new"]({ name: "Command3" })
    expect(store["first"].name).toEqual("Command1")
    expect(store.commands).toHaveLength(3)
    store["shift"]()

    expect(store["first"].name).toEqual("Command2")
    expect(store.commands).toHaveLength(2)
  })

  it("should remove the last element of the state.commands and return it", () => {
    store["new"]({ name: "Command1" })
    store["new"]({ name: "Command2" })
    store["new"]({ name: "Command3" })
    expect(store["last"].name).toEqual("Command3")
    expect(store.commands).toHaveLength(3)
    store["pop"]()

    expect(store["last"].name).toEqual("Command2")
    expect(store.commands).toHaveLength(2)
  })

  it("should remove the element at index", () => {
    store["new"]({ name: "Command1" })
    store["new"]({ name: "Command2" })
    store["new"]({ name: "Command3" })
    expect(store["last"].name).toEqual("Command3")
    expect(store.commands).toHaveLength(3)
    store["splice"]({
      index: 1,
    })

    expect(store["first"].name).toEqual("Command1")
    expect(store["last"].name).toEqual("Command3")
    expect(store.commands).toHaveLength(2)
  })

  it("should not increment position if they are no commands", () => {
    store["position:increment"]()
    expect(store.position).toEqual(0)
    store["position:increment"]()
    store["position:increment"]()
    expect(store.position).toEqual(0)
  })

  it("should reset position to commands length - 1", async () => {
    await store["new"]({ name: "Command1" })
    expect(store.position).toEqual(store.commands.length - 1)
  })
  it("should reset commands to initial", async () => {
    await store["reset"]()
    expect(store.commands).toEqual([])
  })

  it("should reset commands to a slice of history if status === 'HISTORY'", async () => {
    await store["new"]({ name: "Command1" })
    await store["new"]({ name: "Command2" })
    await store["new"]({ name: "Command3" })
    await store["undo:last"]()
    await store["undo:last"]()
    await store["new"]({ name: "Command2" })
    expect(store.commands).toHaveLength(2)
    expect(store.status).toEqual("TOP")
    expect(store.commands[0].name).toEqual("Command1")
    expect(store.commands[1].name).toEqual("Command2")
  })

  it("should commit position:increment if state.position < state.commands.length", async () => {
    await store["new"]({ name: "Command1" })
    await store["new"]({ name: "Command2" })
    await store["undo:last"]()
    await store["undo:last"]()
    await store["execute:last"]()
    expect(store.position).toEqual(0)
  })

  it("should not decrement position if commands.length < 0", async () => {
    await store["new"]({
      name: "Command1",
      undo: () => {
        throw new Error("test")
      },
    })
    try {
      await store["undo:last"]()
    } catch (e) {
      expect(e).toEqual(new Error("test"))
    }
  })

  it("should execute last", async () => {
    await store["new"]({
      name: "Command1",
      execute: () => new Promise((resolve) => resolve("test")),
    })
    await store["undo:last"]()
    const result = await store["execute:last"]()
    expect(store.commands).toHaveLength(1)
    expect(result).toEqual("test")
  })
  it("should reject execute last", async () => {
    try {
      await store["new"]({
        name: "Command1",
        execute: () => new Promise((resolve, reject) => reject()),
      })
    } catch (e) {
      expect(e).toEqual(new Error("top of history"))
    }
  })
  it("should reject with end of history if state has no commands", async () => {
    try {
      await store["execute:last"]()
    } catch (e) {
      expect(e).toBeDefined()
      expect(e).toEqual(new Error("top of history"))
    }
  })
})
