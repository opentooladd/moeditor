import { fetch, Response as TauriResponse } from '@tauri-apps/api/http';
import { defineStore } from "pinia"

const fontUri = "https://fonts.google.com/metadata/fonts";

const webSafeFonts = [
  {
    family: "Arial",
    category: "sans-serif",
    designers: [],
    fonts: {},
    isWebSafe: true
  },
  {
    family: "Verdana",
    category: "sans-serif",
    designers: [],
    fonts: {},
    isWebSafe: true
  },
  {
    family: "Tahoma",
    category: "sans-serif",
    designers: [],
    fonts: {},
    isWebSafe: true
  },
  {
    family: "Trebuchet MS",
    category: "sans-serif",
    designers: [],
    fonts: {},
    isWebSafe: true
  },
  {
    family: "Times New Roman",
    category: "serif",
    designers: [],
    fonts: {},
    isWebSafe: true
  },
  {
    family: "Georgia",
    category: "serif",
    designers: [],
    fonts: {},
    isWebSafe: true
  },
  {
    family: "Garamond",
    category: "serif",
    designers: [],
    fonts: {},
    isWebSafe: true
  },
  {
    family: "Courier New",
    category: "monospace",
    designers: [],
    fonts: {},
    isWebSafe: true
  },
  {
    family: "Brush Script MT",
    category: "cursive",
    designers: [],
    fonts: {},
    isWebSafe: true
  },
]

export interface FontsMetadata {
  familyMetadataList: Font[]
}

export interface Font {
  family: string,
  category: string,
  designers: string[],
  fonts: { [key: string]: any },
  size: number,
  isWebSafe: boolean
}

interface FontStoreState {
  fonts: Font[],
}

export const useFontsStore = defineStore("fonts", {
  state: (): FontStoreState => ({
    fonts: []
  }),
  getters: {},
  actions: {
    async fetch() {
      try {
        if (window.__TAURI__) {
          const result: TauriResponse<FontsMetadata> = await fetch(fontUri);
          const data: FontsMetadata = result.data;
          this.fonts = webSafeFonts.concat(data.familyMetadataList);
        } else {
          const result: Response = await window.fetch(fontUri);
          const data: FontsMetadata = await result.json()
          this.fonts = webSafeFonts.concat(data.familyMetadataList);
        }
      } catch (e) {
        console.error(e);
      }
    }
  }
})
