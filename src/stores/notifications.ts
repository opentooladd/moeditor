import { defineStore } from "pinia"
import { List } from "immutable"
import uuid from "@/libs/micro-outils/dist/lang/uuid"

// @TODO
// Handle mouse over stop timer
// Add progress bar at bottom
// Add Parameters
// Add style

interface Notification {
  type: string
  id: string
  content: string
}

interface NotificationsStoreState {
  items: List<Notification>
}

export const useNotificationsStore = defineStore("notifications", 
  {
    state: (): NotificationsStoreState => ({
      items: List()
    }),
    actions: {
      push(el: any) {
        this.items = this.items.push({ id: uuid(), ...el })
      },
      pop() {
        this.items = this.items.pop()
      },
      unshift(el: any) {
        this.items = this.items.unshift({ id: uuid(), ...el })
        window.setTimeout(() => this.pop(), el.timeout)
      },
      shift() {
        this.items = this.items.shift()
      }
    }
  })
