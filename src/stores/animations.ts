import { ref, type Ref } from "vue";
import { AnimeParams, AnimeAnimParams, AnimeTimelineInstance } from 'animejs';
import anime from 'animejs/lib/anime.es.js';
import { defineStore } from "pinia"

// https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/animejs/index.d.ts

interface TimelineInstance extends AnimeTimelineInstance {
  children?: AnimeElement[],
  remove(elements: AnimeTarget): AnimeTimelineInstance
}

interface AnimationsStoreState {
  timeline: TimelineInstance,
  childrenLength: Ref<number>,
  isUpdated: boolean
}
const timelineParameters: AnimeParams = {
  duration: 5000
};

export const useAnimationsStore = defineStore("animations", () => {
  const timeline = ref(anime.timeline(timelineParameters) as TimelineInstance);
  const childrenLength = ref(0)

  const add = (elements: AnimeTarget, data: AnimeAnimParams = {}, offset = 0) => {
    timeline.value.add({
      targets: elements,
      ...data
    }, offset);
    childrenLength.value = timeline.value.children.length;
  };
  const remove = (elements: AnimeTarget) => {
    timeline.value.remove(elements);
    childrenLength.value = timeline.value.children.length;
  }

  return {
    timeline,
    childrenLength,
    add,
    remove
  }
})
