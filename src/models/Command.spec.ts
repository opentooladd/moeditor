/* eslint func-names: 0 */

import { describe, it, expect, vi } from "vitest"
import Command from "./Command"

describe("Command", () => {
  it("should create a command", () => {
    const subject = {
      test: [],
    }

    const command = new Command({
      execute: function (a: any) {
        return this.meta.test.push(a)
      },
      undo: function () {
        return this.meta.test.pop()
      },
      meta: subject,
    })

    const executeSpy = vi.spyOn(command, "execute")
    const undoSpy = vi.spyOn(command, "undo")

    expect(command).toBeInstanceOf(Command)

    command.execute("Hello")
    expect(executeSpy).toHaveBeenCalledWith("Hello")
    expect(subject).toEqual(command.meta)
    expect(subject.test).toEqual(command.meta.test)

    command.undo()
    expect(undoSpy).toHaveBeenCalled()
    expect(subject).toEqual(command.meta)
    expect(subject.test).toEqual(command.meta.test)

    const testCommand = new Command({
      name: "testCommand",
    })

    expect(testCommand).toBeInstanceOf(Command)
    expect(testCommand.name).toEqual("testCommand")
    expect(testCommand.execute).toBeInstanceOf(Function)
    expect(testCommand.undo).toBeInstanceOf(Function)
    expect(testCommand.meta).toBeInstanceOf(Object)

    expect(() => testCommand.execute()).not.toThrow()
    expect(() => testCommand.undo()).not.toThrow()
  })
})
