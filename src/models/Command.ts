/* eslint-disable */
export default class Command {
  name: string
  execute: (this: Command, ...args: any) => any
  undo: (this: Command, ...args: any) => any
  meta: { [key: string]: any }

  constructor({
    name = "Command",
    execute = function () {},
    undo = function () {},
    meta = {},
  }: CommandArgs) {
    this.name = name
    this.execute = execute
    this.undo = undo
    this.meta = meta
  }
}
