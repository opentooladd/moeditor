import { useNotificationsStore } from "@/stores/notifications"

const defaultParams = {
  timeout: 3000
}

export const notify = (args: any = {}): void => {
  const notificationsStore = useNotificationsStore() 
  notificationsStore.unshift({ ...defaultParams, ...args })
}

export const useNotification = () => ({ notify })
