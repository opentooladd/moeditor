declare interface CommandArgs {
  name?: string;
  execute?: (this: Command, ...args) => any;
  undo?: (this: Command, ...args) => any;
  meta?: { [key: string]: any };
}

declare class Command {
  name: string
  execute: (this: Command, ...args) => any
  undo: (this: Command, ...args) => any
  meta: { [key: string]: any }

  constructor({
    name = "Command",
    execute,
    undo,
    meta = {},
  }: CommandArgs): Command;
}
