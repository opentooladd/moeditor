declare module "splitpanes" {
    let Splitpanes : HtmlTagDescriptor
    let Pane : HtmlTagDescriptor

    export { Splitpanes, Pane }
}
