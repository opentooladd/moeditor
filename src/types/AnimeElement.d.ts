interface AnimeElement {
  update: () => void;
  begin: () => void;
  loopBegin: () => void;
  changeBegin: () => void;
  change: () => void;
  changeComplete: () => void;
  loopComplete: () => void;
  complete: () => void;
  loop: number;
  direction: string;
  autoplay: boolean;
  timelineOffset: number;
  id: number;
  children: AnimeElement[];
  animatables: {
    id: number;
    target: HTMLElement;
    total: number;
    transforms: { [key: string]: any }
  }[];
  animations: anime.Animation[];
  duration: number;
  delay: number;
  endDelay: number;
  finished: Promise<() => void>;
  reset: () => void;
  set: () => void;
  tick: (t: number) => void;
  seek: (time: number) => void;
  pause: () => void;
  play: () => void;
  reverse: () => void;
  restart: () => void;
  remove: (targets: any[]) => void;
  passThrough: boolean;
  currentTime: number;
  progress: number;
  paused: boolean
  began: boolean;
  loopBegan: boolean;
  changeBegan: boolean;
  completed: boolean;
  changeCompleted: boolean;
  reversePlayback: boolean;
  reversed: boolean;
  remaining: number;
}
