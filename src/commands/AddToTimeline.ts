import { useAnimationsStore } from "@/stores/animations";

export default (element: HTMLElement & { initialStyle: any }, offset = 0) => {
  const animationsStore = useAnimationsStore();
  let elementAnimation = null

  return {
    name: "add-to-timeline",
    execute: () => {
      if (!element) throw new Error("element does not exist");
      element.initialStyle = element.style
      animationsStore.add(element, {
        keyframes: [
          { translateX: 0, duration: 1 },
          { translateX: 100, duration: 3000 }
        ]
      }, offset);
    },
    undo: () => {
      if (!element) throw new Error("element does not exist");
      elementAnimation = {
        keyframes: animationsStore.timeline.children?.find((e) => element === e.animatables[0]?.target)?.animations.reduce((acc, animation) => {
          const keyframes = animation.tweens.map((t: any) => ({
            [animation.property]: t.to.original,
            duration: t.duration
          }))
          return acc
        }, [])
      };
      animationsStore.remove(element);
      element.setAttribute("style", element.initialStyle);
    },
  }
}
