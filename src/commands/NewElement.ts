import { useTreeStore } from "@/stores/tree";

export default (data: {[key: string]: string}, target: HTMLElement | null) => {
  const parent = target ? target : window.document.querySelector("#main-scene");
  const treeStore = useTreeStore();
  const id = data.attributes["data-id"];
  const selector = `[data-id="${id}"]`;
  const element = window.document.createElement(data.tag);
  const styleSheet: CSSStyleSheet = Array.from(document.styleSheets).find((stylesheet) => stylesheet.title === "project-css");
  let ruleIndex;

  if (data.attributes) {
    Object.keys(data.attributes).forEach((key) => {
      element.setAttribute(key, data.attributes[key])
    })
  } 

  return {
    name: "new-element",
    execute: () => {
      parent.appendChild(element);
      ruleIndex = styleSheet.insertRule(`${selector} {}`);
      const rule: CSSStyleRule = styleSheet.cssRules.item(ruleIndex) as CSSStyleRule;
      if (data.style) {
        Object.entries(data.style).forEach(([key, value]) => {
          rule.style.setProperty(key, value);
        })
      }
      treeStore.scan();
    },
    undo: () => {
      if (element === treeStore.selected) treeStore.select(null)
      parent.removeChild(element);
      styleSheet.deleteRule(ruleIndex);
      treeStore.scan();
    },
  }
}
