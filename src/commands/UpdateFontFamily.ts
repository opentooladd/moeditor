import { useTreeStore } from "@/stores/tree";
import { useFontsStore, type Font } from "@/stores/fonts";

const getElementStyleDeclaration = (styleSheet: CSSStyleSheet, selector: string): CSSStyleDeclaration | undefined => (Array.from(styleSheet.cssRules).find((rule : CSSStyleRule) => rule.selectorText === selector) as CSSStyleRule)?.style;

const googleFontBaseUri = "https://fonts.googleapis.com/css2?family=";

// <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,700;0,900;1,100;1,400&display=swap" rel="stylesheet"> 
const createGoogleLinkUri = (font: Font): string => {
  const family = font.family;
  const styles = Object.keys(font.fonts);
  const hasItalic = styles.some((style) => style.includes("i"));
  const weights = font.fonts.length > 1 ? `:${hasItalic ? "ital," : ""}wght@` : "";
  const weightStyles = font.fonts.length > 1 ? styles.reduce((acc, style, index, list) => {
    acc += `${style.includes("i") ? "1" : "0"},${style.replace("i", "")}${index < list.length - 1 ? ";" : ""}`;
    return acc
  }, "") : "";
  return `${googleFontBaseUri}${family}${weights}${weightStyles}&display=swap`
}

export default (data: {[key: string]: string}) => {
  const treeStore = useTreeStore();
  const fontsStore = useFontsStore();
  const element = treeStore.selected;
  const id = element.getAttribute("data-id");
  const selector = `[data-id="${id}"]`;
  const linkFontSelector = `google-font-${id}`;
  const styleSheet: CSSStyleSheet = Array.from(document.styleSheets).find((stylesheet) => stylesheet.title === "project-css");
  const InitElementStyle: CSSStyleDeclaration = getElementStyleDeclaration(styleSheet, selector);
  const entries = Object.entries(data);
  if (!element) throw Error("no element selected"); 
  if (!InitElementStyle) throw Error(`no style associated to element: ${id}`);

  const oldRules: {[key: string]: string} = entries.reduce((acc, [key, _]) => {
    acc[key] = InitElementStyle.getPropertyValue(key);
    return acc
  }, {});

  const oldGoogleFontStyle = document.head.querySelector(`#${linkFontSelector}`);

  const googleFontLink = document.createElement("link");
  googleFontLink.rel = "stylesheet";
  googleFontLink.id = linkFontSelector;
  googleFontLink.setAttribute("is-google-font-link", "");
  const fontStoreFont = fontsStore.fonts.find((f) => f.family === data["font-family"]);
  const href = createGoogleLinkUri(fontStoreFont);
  googleFontLink.href = href;

  return {
    name: "update-font-family",
    execute: () => {
      if (oldGoogleFontStyle) document.head.removeChild(oldGoogleFontStyle);
      if (!element) throw new Error("element does not exist");
      const elementStyle = getElementStyleDeclaration(styleSheet, selector);
      if (!elementStyle) throw Error(`no style associated to element: ${id}`);
      if (!fontStoreFont.isWebSafe) document.head.appendChild(googleFontLink);
      entries.forEach(([key, value]) => elementStyle.setProperty(key, value));
    },
    undo: () => {
      if (!element) throw new Error("element does not exist");
      const elementStyle = getElementStyleDeclaration(styleSheet, selector);
      if (!elementStyle) throw Error(`no style associated to element: ${id}`);
      if (!fontStoreFont.isWebSafe) document.head.removeChild(googleFontLink);
      if (oldGoogleFontStyle) document.head.appendChild(oldGoogleFontStyle);
      Object.entries(oldRules).forEach(([key, value]) => {
        if (value === "") elementStyle.removeProperty(key);
        else elementStyle.setProperty(key, value);
      });
    }
  }
}
