import { useTreeStore } from "@/stores/tree"

export default (id: string, toId: string) => {
  const parent = toId !== "root" ?
    window.document.querySelector(`.multimediaobject[data-id="${toId}"]`) :
    window.document.querySelector("#main-scene")
  const treeStore = useTreeStore()
  const element = window.document.querySelector(`.multimediaobject[data-id="${id}"]`)
  const oldParent = element.parentElement

  return {
    name: "move-element",
    execute: () => {
      parent.appendChild(element);
      treeStore.scan();
    },
    undo: () => {
      oldParent.appendChild(element);
      treeStore.scan();
    },
  }
}
