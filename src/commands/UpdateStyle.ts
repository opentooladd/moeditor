import { useTreeStore } from "@/stores/tree";

const getElementStyleDeclaration = (styleSheet: CSSStyleSheet, selector: string): CSSStyleDeclaration | undefined => (Array.from(styleSheet.cssRules).find((rule : CSSStyleRule) => rule.selectorText === selector) as CSSStyleRule)?.style;

export default (data: {[key: string]: string}) => {
  const treeStore = useTreeStore();
  const element = treeStore.selected;
  const id = element.getAttribute("data-id");
  const selector = `[data-id="${id}"]`;
  const styleSheet: CSSStyleSheet = Array.from(document.styleSheets).find((stylesheet) => stylesheet.title === "project-css");
  const InitElementStyle: CSSStyleDeclaration = getElementStyleDeclaration(styleSheet, selector);
  const entries = Object.entries(data);
  if (!element) throw Error("no element selected"); 
  if (!InitElementStyle) throw Error(`no style associated to element: ${id}`);

  const oldRules: {[key: string]: string} = entries.reduce((acc, [key, _]) => {
    acc[key] = InitElementStyle.getPropertyValue(key);
    return acc
  }, {});

  return {
    name: "update-style",
    execute: () => {
      if (!element) throw new Error("element does not exist");
      const elementStyle = getElementStyleDeclaration(styleSheet, selector);
      if (!elementStyle) throw Error(`no style associated to element: ${id}`);
      entries.forEach(([key, value]) => elementStyle.setProperty(key, value));
    },
    undo: () => {
      if (!element) throw new Error("element does not exist");
      const elementStyle = getElementStyleDeclaration(styleSheet, selector);
      if (!elementStyle) throw Error(`no style associated to element: ${id}`);
      Object.entries(oldRules).forEach(([key, value]) => {
        if (value === "") elementStyle.removeProperty(key);
        else elementStyle.setProperty(key, value);
      });
    },
  }
}
