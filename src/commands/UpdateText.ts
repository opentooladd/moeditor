import { useTreeStore } from "@/stores/tree";

export default (data: {[key: string]: string}) => {
  const treeStore = useTreeStore();
  const element = treeStore.selected;

  const oldText = element.innerText

  return {
    name: "update-text",
    execute: () => {
      if (!element) throw new Error("element does not exist");
      element.innerText = data.text
    },
    undo: () => {
      if (!element) throw new Error("element does not exist");
      element.innerText = oldText
    },
  }
}
