export default ({
  url,
  queries,
  method,
  body,
  headers,
  mode,
  credentials,
}: HttpRequestParameters) =>
  new Promise((resolve, reject) => {
    const uri = `${url}${
      queries
        ? `?${Object.keys(queries).reduce(
          (acc, q) => `${acc}${acc.length > 0 ? "&" : ""}${q}=${queries[q]}`,
          ""
        )}`
        : ""
    }`
    fetch(uri, {
      method,
      body: body as BodyInit,
      headers,
      mode: mode as RequestMode,
      credentials: credentials as RequestCredentials,
    })
      .then((res) => res.json().then(resolve).catch(reject))
      .catch(reject)
  })
