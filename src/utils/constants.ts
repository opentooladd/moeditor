export const positionOptions = [
  { value: "static", label: "static" },
  { value: "relative", label: "relative" },
  { value: "absolute", label: "absolute" },
  { value: "fixed", label: "fixed" },
  { value: "sticky", label: "sticky" },
]
