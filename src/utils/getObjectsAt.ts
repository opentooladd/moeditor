export default (position: { x: number, y: number } = { x: 0, y: 0 }, array: any[] = []) => {
  return array.filter((el: HTMLElement) => {
    const rect = el.getBoundingClientRect()
    return (rect.left <= position.x && position.x <= rect.right)
          && (rect.top <= position.y && position.y <= rect.bottom)
  })
}
