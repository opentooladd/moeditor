import getObjectsAt from "./getObjectsAt"

describe("mixins: getObjectAt", () => {
  const testingElements = [{
    name: "box0",
    getBoundingClientRect: () => ({
      top: 100,
      left: 20,
      right: 40,
      bottom: 200,
    }),
  }, {
    name: "box1",
    getBoundingClientRect: () => ({
      top: 300,
      left: 170,
      right: 300,
      bottom: 400,
    }),
  }, {
    name: "box2",
    getBoundingClientRect: () => ({
      top: 800,
      left: 20,
      right: 70,
      bottom: 900,
    }),
  }, {
    name: "box3",
    getBoundingClientRect: () => ({
      top: 50,
      left: 278,
      right: 356,
      bottom: 200,
    }),
  }, {
    name: "box4",
    getBoundingClientRect: () => ({
      top: 70,
      left: 354,
      right: 785,
      bottom: 73,
    }),
  }]
  const cursorPositions = [
    undefined,
    {
      x: 15,
      y: 150,
    },
    {
      x: 178,
      y: 320,
    },
    {
      x: 378,
      y: 72,
    },
  ]
  const expectations = [
    [],
    [],
    [testingElements[1]],
    [testingElements[4]],
  ]
  const testFunction = function testFunction(ret: any, index: number) {
    it(`should return ${JSON.stringify(expectations[index])} for cursor position ${JSON.stringify(ret)}`, () => {
      expect(ret).toEqual(expectations[index])
    })
  }
  cursorPositions.forEach((cursor, index) => {
    testFunction(getObjectsAt(cursor, testingElements), index)
  })
  testFunction(getObjectsAt(), 0)
})
