SHELL := /bin/bash
UTILS_FOLDER := ~/Nextcloud/BASH_UTILS/
SERVER_URI := http://localhost:5173
CONTAINER_MANAGER := podman
PROJECT_PATH := "."

all: install vite-dev
	# make -j2 tauri-dev open-server

install:
	git submodule update --init --recursive
	npm install

watch-tauri-dev:
	watchexec -r -w src make tauri-run

tauri-dev:
	unset APPDIR && unset APPIMAGE && npm run tauri dev

vite-dev:
	npm run dev

server: build
	npm run server

wait-for-server:
	@$(UTILS_FOLDER)wait_for_url.sh "$(SERVER_URI)"

open-server: server wait-for-server
	xdg-open $(SERVER_URI)

tauri-run:
	npm run build
	unset APPDIR && unset APPIMAGE && cd ./src-tauri && cargo run

build:
	npm run build

build-tauri-dev:
	cd ./src-tauri && cargo build

build-tauri-debug: src-tauri/target/debug/app

build-tauri-release: src-tauri/target/release/app
	
update-packages:
	npm install @tauri-apps/cli@latest @tauri-apps/api@latest

tests: test-unit test-e2e

test-unit:
	npm run "test:unit"

test-e2e:
	make -j2 server open-cypress

open-cypress:
	npm run "test:e2e"

copy-coverage: clean-coverage
	mkdir ./coverage/multimediaobject && cp -r src/libs/multimediaobject/coverage/lcov-report/* ./coverage/multimediaobject
	mkdir ./coverage/multimediaobject-drivers && cp -r src/libs/multimediaobject-drivers/coverage/lcov-report/* ./coverage/multimediaobject-drivers

clean-coverage:
	rm -rf ./coverage/multimediaobject
	rm -rf ./coverage/multimediaobject-drivers

	cp ./src/libs/multimediaobject-drivers/dist/$(@F) ./tests/libs/$(@F)

src-tauri/target/debug/app: src-tauri/src/main.rs dist/index.html
	# works in independent terminal
	# don't know why not here
	# maybe something with acces rights ?
	npm run tauri build -- --debug

src-tauri/target/release/app: src-tauri/src/main.rs dist/index.html
	# works in independent terminal
	# don't know why not here
	# maybe something with acces rights ?
	npm run tauri build -- --verbose

dist/index.html:
	npm run build

clean-frontend:
	rm -rf dist/

install-watchexec:
	cargo install watchexec-cli

clean-tauri:
	rm -rf ./src-tauri/target

clean: clean-frontend clean-tauri
