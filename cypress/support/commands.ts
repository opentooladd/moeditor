/// <reference types="cypress" />
// ***********************************************
// This example commands.ts shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
//
// declare global {
//   namespace Cypress {
//     interface Chainable {
//       login(email: string, password: string): Chainable<void>
//       drag(subject: string, options?: Partial<TypeOptions>): Chainable<Element>
//       dismiss(subject: string, options?: Partial<TypeOptions>): Chainable<Element>
//       visit(originalFn: CommandOriginalFn, url: string, options: Partial<VisitOptions>): Chainable<Element>
//     }
//   }
// }

Cypress.Commands.add(
  "testCSSValue",
  {
    prevSubject: true,
  },
  (subject, cssProperty: string, isNumber: boolean, chaiTest: Function) => {
    cy.wrap(subject).should((el) => {
      const value = el.css(cssProperty);
      const stringValue = value.toString();
      if (isNumber) {
        const matchedNumbers: Number[] | undefined = stringValue
          .match(/(\d\.\d+)+|(\d+)+/g)
          ?.map((n) => Number(n));

        chaiTest(matchedNumbers);
      } else {
        chaiTest(stringValue);
      }
      return subject
    });
  }
);

declare global {
  namespace Cypress {
    interface Chainable {
      testCSSValue(
        cssProperty: string,
        isNumber: boolean,
        chaiTest: Function
      ): Chainable<void>;
    }
  }
}

export {};
