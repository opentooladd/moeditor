import { expect } from "chai"

describe("create DOM element", () => {
  beforeEach(() => {
    cy.visit("/")
    cy.get(".tree-container").rightclick("center")
    cy.get(".menu-element__list__item").contains("New").click()
  })
  it("should create a DOM element with a right click", () => { 
    cy.get(".dom-tree-item").should("have.length", 1)
    cy.get(".dom-tree-item").contains("multimediaobject")

    cy.get(".multimediaobject").should("have.length", 1)
  })
  it("should have the default css", () => {
    cy.get(".multimediaobject")
      .testCSSValue("background-color", false, (value: string) => expect(value).to.eq("rgb(0, 0, 0)"))
      .testCSSValue("width", true, (value: number[]) => expect(value[0]).to.eq(50))
      .testCSSValue("height", true, (value: number[]) => expect(value[0]).to.eq(50))
      .testCSSValue("position", false, (value: string) => expect(value).to.eq("static"))
  })

  it("should create an element within another one", () => {
    cy.get(".dom-tree-item").first().rightclick()
    cy.get(".menu-element__list__item").contains("New").click()

    cy.get(".item").first().find(".arrow")
      .testCSSValue("transform", false, (value: string) => expect(value).to.eq("matrix(0.707107, -0.707107, 0.707107, 0.707107, 0, 0)"))
    cy.get(".item").click()

    cy.get(".item .dom-tree-item").first()
      .testCSSValue("background-color", false, (value: string) => expect(value).to.eq("rgb(89, 149, 237)"))
      .testCSSValue("color", false, (value: string) => expect(value).to.eq("rgb(255, 255, 255)"))
    cy.get(".item .arrow")
      .testCSSValue("transform", false, (value: string) => expect(value).to.eq("matrix(0.707107, 0.707107, -0.707107, 0.707107, 0, 0)"))

    cy.get(".multimediaobject.selected")
      .testCSSValue("box-shadow", false, (value: string) => expect(value).to.eq("rgb(104, 209, 111) 0px 0px 6px 8px"))

    cy.get(".tree-container > :nth-child(1) > .list-container > .list > .item")
      .rightclick()

    cy.get(".menu-element__list__item").contains("New").click()

    cy.get(".tree-container > :nth-child(1) > .list-container > .list > .item").click()

    cy.get(":nth-child(2) > .list-container > .list > .item")

    cy.get(".tree-container").rightclick("center")
    cy.get(".menu-element__list__item").contains("New").click()

    cy.get("[tabindex='-1']").click()
  })
})
